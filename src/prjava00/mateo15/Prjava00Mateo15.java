/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prjava00.mateo15;


import java.net.InetAddress;
import java.net.UnknownHostException;

public class Prjava00Mateo15 {


                
public static void main(String[] args) {
		// TODO code application logic here
		System.out.println("Projecte de Jordi Mateo");
		System.out.println("versió 0.2 del projecte prjava00-Mateo15");
		try {
			InetAddress addr = InetAddress.getLocalHost();
			byte[] ipAddr = addr.getAddress();
			String hostname = addr.getHostName();
			System.out.println("hostname="+hostname);
			System.out.println("Nom de l'usuari: " + System.getProperty("user.name"));
			System.out.println("Carpeta Personal: " + System.getProperty("user.home"));
			System.out.println("Sistema operatiu: " + System.getProperty("os.name"));
			System.out.println("Versió OS: " + System.getProperty("os.version"));
		}
		catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
    
}
